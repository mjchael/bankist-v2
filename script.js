'use strict';

const btnScrollTo = document.querySelector('.btn--scroll-to');
const section1 = document.querySelector('#section--1');
const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');
const header = document.querySelector('.header');
const message = document.createElement(`div`);
const tabs = document.querySelectorAll('.operations__tab');
const tabContainer = document.querySelector('.operations__tab-container');
const tabContent = document.querySelectorAll('.operations__content');
const nav = document.querySelector('.nav');


// Modal window
const openModal = function (e) {
  e.preventDefault();
  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

const closeModal = function (e) {
  e.preventDefault();
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

btnsOpenModal.forEach(btn => btn.addEventListener('click', openModal));

btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});

// page Navigation
// following example is not a good practice (have performance issue)
// document
//     .querySelectorAll('.nav__link')
//     .forEach(
//         (el) => el.addEventListener(
//             'click', function(e){
//               e.preventDefault();
//               const id = this.getAttribute('href');
//               document.querySelector(id).scrollIntoView({behavior: 'smooth'});
//             }
//         )
//     );
// page Navigation - event delegation
// 1. add event listener to common parent element
// 2. determine what element originated the event
document.querySelector('.nav__links').addEventListener(
  'click', function(e){
      e.preventDefault();
      if (e.target.classList.contains('nav__link')){
        const id = e.target.getAttribute('href');
        if (id !== '#')
          document.querySelector(id).scrollIntoView({behavior: 'smooth'});
      }
    }
);

// tabbed component

tabContainer.addEventListener(
  'click', function(e){
      const clicked = e.target.closest('.operations__tab'); // fix issue when clickin span.]
      if (!clicked) return; // guard clause

      // remove active classes
      tabs.forEach(tab => tab.classList.remove('operations__tab--active'));
      tabContent.forEach(tab =>  tab.classList.remove('operations__content--active'));

      // set active tab
      clicked.classList.add('operations__tab--active');
      document.querySelector(`.operations__content--${clicked.dataset.tab}`).classList.add('operations__content--active')
    }
);
let es;
// Menu fade animation

const handleHover = function(e){
  if (e.target.classList.contains('nav__link')) {
    const link = e.target;
    const siblings = link.closest('.nav').querySelectorAll('.nav__link');
    const logo = link.closest('.nav').querySelector('img');
    siblings.forEach(sibling => {
      if (sibling !== link) {
        sibling.style.opacity = this;
      }
    });
    logo.style.opacity = this;
  }
}

nav.addEventListener('mouseover', handleHover.bind(0.5));

nav.addEventListener('mouseout', handleHover.bind(1));

// cookie message
message.classList.add('cookie-message');
message.innerHTML = `We use cookies for improved functionality & analytics.<button class="btn btn--close-cookie">Got it!</button>`;
header.append(message);
document.querySelector('.btn--close-cookie').addEventListener(
    'click', ()=> message.remove()
);

message.style.backgroundColor='#37383d';
message.style.width = '120%';
message.style.height = Number.parseFloat(getComputedStyle(message).height, 10) + 40 + 'px';

// smooth scrolling
btnScrollTo.addEventListener(
    'click', function (e) {
      const section1Coordinates = section1.getBoundingClientRect();
      section1.scrollIntoView({behavior: 'smooth'});
    }
);

// sticky navigation : Intersection Observer API
const navHeight = nav.getBoundingClientRect().height;
const stickyNav = function (entries) {
    const [ entry ]= entries; // same as entries[0]
    if (!entry.isIntersecting){
        nav.classList.add('sticky');
    } else {
        nav.classList.remove('sticky');
    }
};

const headerObserver = new IntersectionObserver(stickyNav, {
    root: null,
    threshold: 0,
    rootMargin: `-${navHeight}px`
});
headerObserver.observe(header);

// reveal sections
const allSections = document.querySelectorAll('.section');
const revealSection = function(entries, observer){
    const [entry] = entries;
    if (!entry.isIntersecting) return;
    if (entry.isIntersecting) entry.target.classList.remove('section--hidden');
    observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(revealSection, {
    root: null,
    threshold: 0.15
});
allSections.forEach(section => {
    sectionObserver.observe(section);
    // section.classList.add('section--hidden');
});

// lazy loading images
const imgTargets = document.querySelectorAll('img[data-src]');
const loadImg = function(entries, observer) {
    const [entry] = entries;

    if (!entry.isIntersecting) return;

    // replace src attr with data.src
    entry.target.src = entry.target.dataset.src;
    entry.target.addEventListener(
        'load', function(){
            entry.target.classList.remove('lazy-img');
        }
    );
    observer.unobserve(entry.target);
};
const imgObserver  = new IntersectionObserver(loadImg, {
    root: null,
    threshold: 0,
    rootMargin : '+200px'
});
imgTargets.forEach(img=> imgObserver.observe(img));

// slider
const slider = function(){
    const slides = document.querySelectorAll('.slide');
    const btnLeft = document.querySelector('.slider__btn--left');
    const btnRight = document.querySelector('.slider__btn--right');

    const dotContainer = document.querySelector('.dots');
    const createDots =  () => {
        slides.forEach((_, i)=>{
            dotContainer.insertAdjacentHTML(
                'beforeend',
                `<button class="dots__dot" data-slide="${i}"></button>`
            );
        })
    };

    const activateDot = function(slide){
        document.querySelectorAll('.dots__dot').forEach(
            dot => dot.classList.remove('dots__dot--active')
        );

        document.querySelector(`.dots__dot[data-slide="${slide}"]`).classList.add('dots__dot--active');
    };

    let currentSlide = 0;
    const maxSlide = slides.length-1;

    const goToSlide = (currentSlide) => {
        slides.forEach((s, i) => s.style.transform = `translateX(${(i-currentSlide)*100}%)`);
        activateDot(currentSlide);
    };

    const nextSlide = () => {
        if (currentSlide === maxSlide) {
            currentSlide=0;
        }else{
            currentSlide++;
        }
        goToSlide(currentSlide);
    };
    const previousSlide = () => {
        if (currentSlide === 0) {
            currentSlide=slides.length-1;
        }else{
            currentSlide--;
        }
        goToSlide(currentSlide);
    };



    btnRight.addEventListener('click', nextSlide);
    btnLeft.addEventListener('click', previousSlide);

    document.addEventListener(
        'keydown', function(e){
            if (e.key === 'ArrowLeft') previousSlide();
            if (e.key === 'ArrowRight') nextSlide();
        }
    );

    dotContainer.addEventListener(
        'click', function (e) {
            if (e.target.classList.contains('dots__dot')){
                const {slide} = e.target.dataset;
                goToSlide(slide);
            }
        }
    );

    const init = () => {
        createDots();
        goToSlide(0);

    };
    init();
};
slider();

// const obsCallback = function (entries, observer) {
//     entries.forEach( entry => console.log(entry));
//
// };
//
// const obsOptions = {
//     root: null,
//     threshold: [0, 1,  0.2]
// };
//
// const observer = new IntersectionObserver(obsCallback, obsOptions);
// observer.observe(section1);

// sticky navigation
// const initialCoords = section1.getBoundingClientRect();
//
// // scroll not efficient
// window.addEventListener(
//     'scroll', function () {
//         if (window.scrollY > initialCoords.top) nav.classList.add('sticky');
//         else  nav.classList.remove('sticky');
//     }
// );


// bubbling /capturing
// const randomInt = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);
// const randomColor = () => `rgb(${randomInt(0,255)},${randomInt(0,255)},${randomInt(0,255)})`;
//
// document.querySelector('.nav__link').addEventListener(
//   'click',   function(e){
//       this.style.backgroundColor = randomColor();
//       console.log('link', e.target, e.currentTarget);
//       // e.stopPropagation();
//     }
// );
// document.querySelector('.nav__links').addEventListener(
//     'click',   function(e){
//       this.style.backgroundColor = randomColor();
//       console.log('link', e.target, e.currentTarget);
//
//     }
// );
// document.querySelector('.nav').addEventListener(
//     'click',   function(e){
//       this.style.backgroundColor = randomColor();
//       console.log('link', e.target, e.currentTarget);
//
//     }
// );

/////////
// console.log(document.documentElement);
// console.log(document.head);
// console.log(document.body);
//
// const allSections = document.querySelectorAll('.sections');
// console.log(allSections);
// const allButtons = document.getElementsByTagName('button');
//
// console.log(allButtons);
//
//
// // document.documentElement.style.setProperty('--color-primary', 'orangered');
//
// const logo = document.querySelector('.nav__logo');
// console.log(logo.alt);
// console.log(logo.src);
// console.log(logo.className);
// console.log(logo.getAttribute('designer'));
// console.log(logo.getAttribute('src'));
// console.log(logo.dataset.versionNumber);

// const h1 = document.querySelector('h1');
// const alertH1 = function(e){
//   alert('AddEventListener: Great you are reading the heading :D');
// };
// h1.addEventListener(
//     'mouseenter', alertH1
// );
//
//
// h1.removeEventListener('mouseenter', alertH1);

// h1.onmouseenter = function(e){
//   alert('AddEventListener: Great you are reading the heading :D');
// };

// const h1 = document.querySelector('h1');
//
// // going downwards: child
// console.log(h1.querySelectorAll('.highlight'));
// console.log(h1.childNodes);
// console.log(h1.children);
// h1.firstElementChild.style.color = 'white';
// h1.lastElementChild.style.color = 'red';
// // going upwards : parents
// console.log(h1.parentNode);
// console.log(h1.parentElement);
// h1.closest('.header').style.background='var(--gradient-secondary)';
// h1.closest('h1').style.background='var(--gradient-primary)';
// // going side ways: siblings
// console.log(h1.previousElementSibling);
// console.log(h1.nextElementSibling);
// console.log(h1.previousSibling);
// console.log(h1.nextSibling);
//
// console.log(h1.parentElement.children);
